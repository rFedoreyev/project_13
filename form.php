<!DOCTYPE html>

<head>
  <link rel="stylesheet" href="style.css">
  <meta charset="utf-8">
  <title>Retuen to WEB</title>
</head>
<body >
  <header>
  <div class="banner">
  <a href="#home"><img id="logo" src="images/star.jpg"  alt="Логотип"/></a>

  <h1>Welcome</h1>
</div>
</header>

<div class="container">
  <div class="forma">
  <h2 id="form">Form</h2>

  <form action="" method="POST">
    <ol>
      <li>ФИО: <input name="fio" placeholder="Введите ФИО" /> </li>
      <li>E-mail: <input name="email" placeholder="Введите email" type="email" /></li>
      <li>Дата рождения: <input name="yob" value="2001-07-24" type="date"/> </li>
      <li>
        Пол:
        <input type="radio" checked="checked" name="gender" value="man"/> Муж
        <input type="radio" name="gender" value="woman"/> Жен
        <input type="radio" name="gender" value="another"/> Не скажу
      </li>
      <li>
        Количество конечностей:
        <input type="radio" name="n_limbs" value="1"/>1
        <input type="radio" name="n_limbs" value="2"/>2
        <input type="radio" name="n_limbs" value="3"/>3
        <input type="radio" checked="checked" name="n_limbs" value="4"/>4
        <input type="radio" name="n_limbs" value="5"/>5
      </li>
      <li>
        Сверхспособности:
        <select name="sp-sp[]" multiple="multiple" >
          <option value="immortality">бессмертие</option>
          <option value="passing_through_walls">прохождение сквозь стены</option>
          <option value="levitation">левитация</option>
          <option value="wonder">суперсила</option>
        </select>
      </li>
      <li>
        Биография:<textarea name="bio" placeholder="Введите свою биографию" ></textarea>
      </li>
      <li>
        <input type="checkbox" name="Galochka" />С условиями ознакомлен(а)
      </li>
      <li>
        <input type="submit" value="Отправить" />
      </li>
    </ol>
  </form>
</div>
</div>
<footer>
<h2>Bye</h2>
</footer>

</body>
</html>